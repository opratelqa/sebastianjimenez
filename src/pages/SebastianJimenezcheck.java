package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseTG;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class SebastianJimenezcheck extends TestBaseTG {
	
	final WebDriver driver;
	public SebastianJimenezcheck(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("loader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "ACA VA EL ID DEL BTN CONTINUAR")
	private WebElement btnContinuar; //luego del web element dar el nombre que se desee al id para poder identificarlo facilmente

	@FindBy(how = How.XPATH,using = "ACA VA EL XPATH")
	private WebElement horaInicialRecepcion; //luego del web element dar el nombre que se desee al XPATH para poder identificarlo facilmente

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInSebastianJimemez(String apuntaA, String ambiente) {
		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Sebastian Jimenez - landing");
		
		String URL0 = driver.getCurrentUrl();
		Assert.assertEquals(URL0, apuntaA + ambiente + "/?msisdn=-1" );
		cargando(10);
		espera(500);

	WebElement menu1 = driver.findElement(By.xpath("//a[contains(text(), 'Veterinario en tu casa')]"));
		menu1.click();
		cargando(10);
		String URL1 = driver.getCurrentUrl();
			Assert.assertEquals(URL1, apuntaA + "sebastianjimenez.cl/veterinario-en-tu-casa/" );
			espera(500); 	
	
	WebElement menu2 = driver.findElement(By.xpath("//a[contains(text(), 'Conoce a tu mascota')]"));
		menu2.click();
		cargando(10);
		String URL2 = driver.getCurrentUrl();
			Assert.assertEquals(URL2, apuntaA + "sebastianjimenez.cl/noticias/" );
			espera(500);
	
	WebElement menu3 = driver.findElement(By.xpath("//a[contains(text(), 'Petfriendly')]"));
		menu3.click();
		cargando(10);
		String URL3 = driver.getCurrentUrl();
				Assert.assertEquals(URL3, apuntaA + "sebastianjimenez.cl/petfriendly/" );
				espera(500);
				
	WebElement menu4 = driver.findElement(By.xpath("//a[contains(text(), 'Adopciones')]"));
		menu4.click();
		cargando(10);
		String URL4 = driver.getCurrentUrl();
				Assert.assertEquals(URL4, apuntaA + "sebastianjimenez.cl/adopciones/" );
				espera(500);
				
	//WebElement menu5 = driver.findElement(By.xpath("//a[contains(text(), 'Perdidas y encontradas')]"));
		//menu5.click();
		//cargando(10);
		//String URL5 = driver.getCurrentUrl();
			//	Assert.assertEquals(URL5, apuntaA + "sebastianjimenez.cl/perdidas-y-encontradas/" );
				//espera(500);

				
	
	System.out.println("Resultado Esperado:Deberan visualizarce las secciones de Sebastian Jimenez"); 
	System.out.println();
	System.out.println("Fin de Test Sebastian Jimenez - Landing");
		

	}		

}  

